<?php

/**
 *
 */
class Equipos extends CI_Controller
{
  function __construct()
  {
    parent::__construct();
    $this->load->model("Equipo");
    //deshabilitando errores y advertencias de PHP
    error_reporting(0);

  }
  public function index(){

    $data["listadoEquipos"]=
                    $this->Equipo->consultarTodos();
    $this->load->view("header");
    $this->load->view("equipos/index",$data);
    $this->load->view("footer");
  }
  public function borrar($id_equi){
    $this->Equipo->eliminar($id_equi);
    $this->session->set_flashdata("confirmacion", "Equipo eliminado existosamente");
    redirect('equipos/index');
}



  //Renderizando hospitales
  public function nuevo(){
      $this->load->view("header");
      $this->load->view("equipos/index", $data);
      $this->load->view("footer");
  }

  //Renderizar el formulario de edicion
  public function editar($id_equi){
      $this->load->model("Equipo");
      $data["equipoEditar"] = $this->Equipo->obtenerPorId($id_equi);
      $this->load->view("header");
      $this->load->view("equipos/editar", $data);
      $this->load->view("footer");
  }



  //insertar hospitales
  public function guardarEquipo(){

    $datosNuevosEquipo=array(
      "id_equi" => $this->input->post("id_equi"),
      "nombre_equi" => $this->input->post("nombre_equi"),
      "siglas_equi" => $this->input->post("siglas_equi"),
      "fundacion_equi" => $this->input->post("fundacion_equi"),
      "region_equi" => $this->input->post("region_equi"),
      "numero_titulos_equi" => $this->input->post("numero_titulos_equi")

    );
    $this->Equipo->insertar($datosNuevosEquipo);
      //flash crear una sesion tipo flash
      $this->session->set_flashdata("confirmacion","equipo guardado exitosamente");
      redirect('equipos/index');
  }

  public function actualizarEquipo()
{
 $id_equi = $this->input->post("id_equi");

 // Obtener los datos actuales de la agencia
 $equipoEditar = $this->Equipo->obtenerPorId($id_equi);

 // Obtener los datos actualizados del formulario
 $datosEquipo = array(
   "id_equi" => $this->input->post("id_equi"),
   "nombre_equi" => $this->input->post("nombre_equi"),
   "siglas_equi" => $this->input->post("siglas_equi"),
   "fundacion_equi" => $this->input->post("fundacion_equi"),
   "region_equi" => $this->input->post("region_equi"),
   "numero_titulos_equi" => $this->input->post("numero_titulos_equi")
 );



 // Actualizar la agencia con los nuevos datos
 $this->Equipo->actualizar($id_equi, $datosEquipo);

 // Flash message
 $this->session->set_flashdata("confirmacion", "Equipo actualizado exitosamente");

 // Redireccionar a la página de lista de agencias
 redirect('equipos/index');
}





}



 ?>
