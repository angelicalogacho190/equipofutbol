<div style="padding: 150px 70px 20px 100px">
    <div class="text-center">
        <h1><i class="fa-solid fa-book"></i>&nbsp;&nbsp;POSICIÓN</h1>
    </div>
    <div class="row">
        <div class="col-md-12 text-end">
            <button type="button" class="btn btn-outline-primary" data-bs-toggle="modal" data-bs-target="#exampleModal">
                <i class="fa fa-plus-circle fa-1x"></i> Agregar nuevo Posición
            </button>
        </div>
    </div>
    <br>

    <?php if ($listadoPosiciones): ?>
    <table class="table table-striped text-center">
        <thead class="table-dark">
            <tr>
                <th>ID</th>
                <th>NOMBRE DE LA POSICIÓN</th>
                <th>DESCRIPCIÓN DE LA POSICIÓN</th>
                <th>ACCIONES</th>
            </tr>
        </thead>
        <tbody>
            <?php foreach ($listadoPosiciones as $posicion): ?>
            <tr>
                <td class="text-dark"><?php echo $posicion->id_pos; ?></td>
                <td class="text-dark"><?php echo $posicion->nombre_pos; ?></td>
                <td class="text-dark"><?php echo $posicion->descripcion_pos; ?></td>

                <td>
                  <a href="<?php echo site_url('posiciones/editar/'.$posicion->id_pos); ?>" class="btn btn-warning" title="Editar">
                    <i class="fa fa-pen"></i>
                  </a>
                  <a href="#" class="btn btn-danger" onclick="eliminarRegistro('<?php echo site_url('posiciones/borrar/'.$posicion->id_pos); ?>')">
                    <i class="fa fa-trash"></i>
                  </a>

                </td>
            </tr>
            <?php endforeach; ?>
        </tbody>
    </table>

    <div class="modal-footer"></div>

    <?php else: ?>
    <div class="alert alert-danger">
        No se encontró la posicion registrado
    </div>
    <?php endif; ?>
</div>

<!-- Modal -->
<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-xl">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">Nueva Posición</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <div class="container">
                    <form class="text-dark" action="<?php echo site_url('posiciones/guardarPosicion') ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_posicion">

                        <div class="mb-3 text-dark">
                            <label for="nombre_pos" class="form-label text-dark"><b>Nombre de la posición:</b></label>
                            <input id="nombre_pos" type="text" name="nombre_pos" value="" oninput="validarLetras(this)" placeholder="Ingrese el nombre de la posicion" class="form-control" required>
                        </div>
                        <div class="mb-3 text-dark">
                            <label for="descripcion_pos" class="form-label text-dark"><b>Descripción de la Posición:</b></label>
                            <input id="descripcion_pos" type="text" name="descripcion_pos" value="" oninput="validarLetras(this)" placeholder="Ingrese la descripcion" class="form-control" required>
                        </div>

                        <div class="row justify-content-end">
                            <div class="col-auto">
                                <button type="submit" name="button" class="btn btn-success">
                                    <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbsp;Guardar&nbsp;
                                </button>
                            </div>
                            <div class="col-auto">
                                <a class="btn btn-danger" href="<?php echo site_url('posiciones/index') ?>">
                                    <i class="fa-solid fa-xmark fa-spin"></i>&nbsp;Cancelar&nbsp;
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
function eliminarRegistro(url) {
    Swal.fire({
        title: '¿Estás seguro de eliminar este registro?',
        icon: 'warning',
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: '¡Sí, elimínalo!',
        cancelButtonText: 'Cancelar'
    }).then((result) => {
        if (result.isConfirmed) {
            // Si el usuario confirma la eliminación, redireccionamos a la URL especificada
            window.location.href = url;
        } else {
            // Si el usuario cancela, mostramos un mensaje de cancelación
            Swal.fire(
                'Cancelado',
                'Tu registro no ha sido eliminado :P',
                'error'
            );
        }
    });
}

function validarLetras(input) {
    input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');

}

function validarNumeros(input) {
    input.value = input.value.replace(/\D/g, '');
}
</script>

<style media="screen">
    input {
        color: black !important;
    }
</style>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_posicion").validate({
        rules: {
            "nombre_pos": {
                required: true,
                minlength: 5,
                maxlength: 50,

            },

            "descripcion_pos": {
              required: true,
              minlength: 3,
              maxlength: 20
            }
        },
        messages: {

            "nombre_pos": {
                required: "Debe ingresar el nombre de la posicion",
                minlength: "El nombre debe tener minimo 5 caracteres",
                maxlength: "El nombre no puede tener mas de 50 caracteres"

            },
            "descripcion_pos": {
              required: "Debe ingresar la descripcion de la posicion",
              minlength: "La descripcion debe tener minimo 5 caracteres",
              maxlength: "La descripcion no puede tener mas de 50 caracteres"

            }




        }
    });
});


</script>
