<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR POSICIÓN
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('posiciones/actualizarPosicion'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_posicion">

        <input type="hidden" value="<?php echo $posicionEditar->id_pos; ?>" name="id_pos" id="id_pos">
        <div class="mb-3 text-dark">
            <label for="nombre_pos" class="form-label text-dark"><b>Nombre de la posición:</b></label>
            <input id="nombre_pos" type="text" name="nombre_pos" value="<?php echo $posicionEditar->nombre_pos; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre de la posición" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="descripcion_pos" class="form-label text-dark"><b>Descripción de la posición:</b></label>
            <input id="descripcion_pos" type="text" name="descripcion_pos" value="<?php echo $posicionEditar->descripcion_pos; ?>" oninput="validarLetras(this)" placeholder="Ingrese la descripcion de la posición" class="form-control" required>
        </div>




        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('posiciones/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>

<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_posicion").validate({
        rules: {
            "nombre_pos": {
                required: true,
                minlength: 5,
                maxlength: 50,

            },

            "descripcion_pos": {
              required: true,
              minlength: 3,
              maxlength: 20
            }
        },
        messages: {

            "nombre_pos": {
                required: "Debe ingresar el nombre de la posicion",
                minlength: "El nombre debe tener minimo 5 caracteres",
                maxlength: "El nombre no puede tener mas de 50 caracteres"

            },
            "descripcion_pos": {
              required: "Debe ingresar la descripcion de la posicion",
              minlength: "La descripcion debe tener minimo 5 caracteres",
              maxlength: "La descripcion no puede tener mas de 50 caracteres"

            }




        }
    });
});


</script>
<style media="screen">
    input {
        color: black !important;
    }
</style>