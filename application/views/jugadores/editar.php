<div style="padding: 150px 100px 20px 100px">
<h1>
<b>
  <i class="fa fa-plus-circle"></i>
  EDITAR JUGADOR
</b>
</h1>
<br>

<form class="" action="<?php echo site_url('jugadores/actualizarJugador'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_jugador">


      <div class="mb-3 text-dark">
      <label for="fk_id_equi"><b> Equipo:</b></label>
      <select name="fk_id_equi" id="fk_id_equi" class="form-control" required>
          <option value="">--Seleccione el equipo--</option>
          <?php foreach ($listadoEquipos as $equipo): ?>
            <option value="<?php echo $equipo->id_equi; ?>" <?php if ($equipo->id_equi == $jugadorEditar->fk_id_equi) echo "selected"; ?>>
            <?php echo $equipo->nombre_equi; ?>
          <?php endforeach; ?>
      </select>
        </div>

        <div class="mb-3 text-dark">
    <label for=""><b>Posición:</b></label>
    <select name="fk_id_pos" id="fk_id_pos" class="form-control selectpicker" required>
        <option value="">--Seleccione la posición--</option>
        <?php foreach ($listadoPosiciones as $posicion): ?>
            <option value="<?php echo $posicion->id_pos; ?>" <?php if ($posicion->id_pos == $jugadorEditar->fk_id_pos) echo "selected"; ?>>
           <?php echo $posicion->nombre_pos; ?>
       </option>
        <?php endforeach; ?>
    </select>
      </div>
	<input type="hidden" value="<?php echo $jugadorEditar->id_jug; ?>" name="id_jug" id="id_jug">
  <div class="mb-3 text-dark">
      <label for="apellido_jug" class="form-label text-dark"><b>Apellido:</b></label>
      <input id="apellido_jug" type="text" name="apellido_jug" value="<?php echo $jugadorEditar->apellido_jug; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del jugador" class="form-control" required>
  </div>
    <div class="mb-3 text-dark">
        <label for="nombre_jug" class="form-label text-dark"><b>Nombre:</b></label>
        <input id="nombre_jug" type="text" name="nombre_jug" value="<?php echo $jugadorEditar->nombre_jug; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del jugador" class="form-control" required>
    </div>
    <div class="mb-3 text-dark">
      <label for="estatura_jug" class="form-label text-dark"><b>Estatura:</b></label>
      <input id="estatura_jug" type="text" name="estatura_jug" value="<?php echo $jugadorEditar->estatura_jug; ?>" oninput="validarDecimal(this)" placeholder="Ingrese la estatura en metros (ej. 1.75)" class="form-control" required>
  </div>
    <div class="mb-3">
        <label for="salario_jug" class="form-label"><b>Salario:</b></label>
        <input id="salario_jug" type="text" name="salario_jug" value="<?php echo $jugadorEditar->salario_jug; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el salario" class="form-control" required>
    </div>
    <div class="mb-3">
      <label for="estado_jug" class="form-label"><b>Estado:</b></label>
      <select id="estado_jug" name="estado_jug" class="form-control" required>
          <option value="">Seleccione el Estado</option>
          <option value="Activo" <?php if ($jugadorEditar->estado_jug == "Activo") echo "selected"; ?>>Activo</option>
          <option value="Inactivo" <?php if ($jugadorEditar->estado_jug == "Inactivo") echo "selected"; ?>>Inactivo</option>
      </select>
    </div>

<br>
<div class="row">
  <div class="col-md-12 text-center">
    <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
    &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
    <a class="btn btn-danger" href=" <?php echo site_url('jugadores/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
  </div>

</div>

</form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

function validarDecimal(input) {
input.value = input.value.replace(/[^0-9.]/g, '');
if (input.value.indexOf('.') !== -1) {

  input.value = input.value.substr(0, input.value.indexOf('.') + 3);
}
}

</script>
<script>
      $(document).ready(function() {
        $('#fk_id_equi').select2({
          placeholder: "--Seleccione el equipo--", // Texto del placeholder
          allowClear: true // Opcional, para permitir que se limpie la selección
        });
      });
      </script>
      <script>
            $(document).ready(function() {
              $('#fk_id_pos').select2({
                placeholder: "--Seleccione la posicion--", // Texto del placeholder
                allowClear: true // Opcional, para permitir que se limpie la selección
              });
            });
            </script>


<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_jugador").validate({
        rules: {
          "fk_id_equi": {
              required: true

          },
          "fk_id_pos": {
              required: true

          },
          "apellido_jug": {
              required: true,
              minlength: 4,
              maxlength: 15,

          },
          "nombre_jug": {
              required: true,
              minlength: 4,
              maxlength: 15,

          },
          "estatura_jug": {
              required: true,


          },
          "salario_jug": {
              required: true,
              minlength: 4,
              maxlength: 8,

          },
          "estado_jug": {
              required: true,

          }
        },
        messages: {
          "fk_id_equi": {
              required: "Escoja el equipo"

          },
          "fk_id_pos": {
              required: "Escoja la posicion"

          },
          "apellido_jug": {
            required: "Debe ingresar el apellido del jugador",
            minlength: "El apellido debe tener minimo 4 caracteres",
            maxlength: "El apellido no puede tener mas de 15 caracteres"

          },
          "nombre_jug": {
            required: "Debe ingresar el nombre del jugador",
            minlength: "El nombre debe tener minimo 4 caracteres",
            maxlength: "El nombre no puede tener mas de 15 caracteres"

          },
          "estatura_jug": {
            required: "Debe ingresar la estatura del jugador",


          },
          "salario_jug": {
            required: "Debe ingresar el salario del jugador",
            minlength: "El salario debe tener minimo 4 caracteres",
            maxlength: "El salario no puede tener mas de 6 caracteres"

          },
          "estado_jug": {
            required: "Debe ingresar el estado del jugador",


          }




        }
    });
});


</script>
<style media="screen">
    input {
        color: black !important;
    }
</style>
