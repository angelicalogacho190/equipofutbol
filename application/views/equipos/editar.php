<div style="padding: 150px 70px 20px 100px">

    <h1>
        <b>
            <i class="fa-solid fa-pen-to-square"></i>
            EDITAR EQUIPO
        </b>
    </h1>
    <br>

    <form class="text-dark" action="<?php echo site_url('equipos/actualizarEquipo'); ?>" method="post" enctype="multipart/form-data" id="frm_nuevo_equipo">

        <input type="hidden" value="<?php echo $equipoEditar->id_equi; ?>" name="id_equi" id="id_equi">
        <div class="mb-3 text-dark">
            <label for="nombre_equi" class="form-label text-dark"><b>Nombre del Equipo:</b></label>
            <input id="nombre_equi" type="text" name="nombre_equi" value="<?php echo $equipoEditar->nombre_equi; ?>" oninput="validarLetras(this)" placeholder="Ingrese el nombre del equipo" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="siglas_equi" class="form-label text-dark"><b>Siglas del Equipo:</b></label>
            <input id="siglas_equi" type="text" name="siglas_equi" value="<?php echo $equipoEditar->siglas_equi; ?>" oninput="validarLetras(this)" placeholder="Ingrese las siglas del equipo" class="form-control" required>
        </div>
        <div class="mb-3 text-dark">
            <label for="fundacion_equi" class="form-label text-dark"><b>Fundación del Equipo:</b></label>
            <input id="fundacion_equi" type="text" name="fundacion_equi" value="<?php echo $equipoEditar->fundacion_equi; ?>" oninput="validarNumeros(this)" placeholder="Ingrese la fundación del equipo" class="form-control" required>
        </div>
        <div class="mb-3">
             <label for="region_equi" class="form-label"><b>Región:</b></label>
             <select id="region_equi" name="region_equi" class="form-control" required>
                 <option value="">Seleccione la región</option>
                 <option value="Costa" <?php if ($equipoEditar->region_equi == "Costa") echo "selected"; ?>>Costa</option>
                 <option value="Sierra" <?php if ($equipoEditar->region_equi == "Sierra") echo "selected"; ?>>Sierra</option>
                 <option value="Amazonia" <?php if ($equipoEditar->region_equi == "Amazonia") echo "selected"; ?>>Amazonia</option>
                 <option value="Galapagos" <?php if ($equipoEditar->region_equi == "Galapagos") echo "selected"; ?>>Galápagos</option>
             </select>
         </div>
        <div class="mb-3">
            <label for="numero_titulos_equi" class="form-label"><b>Numero de títulos:</b></label>
            <input id="numero_titulos_equi" type="text" name="numero_titulos_equi" value="<?php echo $equipoEditar->numero_titulos_equi; ?>" oninput="validarNumeros(this)" placeholder="Ingrese el número de títulos" class="form-control" required>
        </div>





        <br>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="submit" name="button" class="btn btn-warning"> <i class="fa-solid fa-floppy-disk fa-bounce"></i>&nbspActualizar&nbsp</button>
                &nbsp;&nbsp;&nbsp&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                <a class="btn btn-danger" href=" <?php echo site_url('equipos/index') ?> "><i class="fa-solid fa-xmark  fa-spin"></i>&nbspCancelar&nbsp</a>
            </div>

        </div>

    </form>

</div>

<script type="text/javascript">
function validarLetras(input) {
  input.value = input.value.replace(/\s+/g, ' ').replace(/[^a-zA-ZñÑ\s]/g, '');


}


function validarNumeros(input) {
input.value = input.value.replace(/\D/g, '');
}

</script>
<script>
  $(document).ready(function () {
    $("#fotografia_nueva").fileinput({
      //showUpload:false
      //showRemove: false,
      language:'es',
    });
  });
</script>


<script type="text/javascript">
$(document).ready(function() {
    $("#frm_nuevo_equipo").validate({
        rules: {
            "nombre_equi": {
                required: true,
                minlength: 5,
                maxlength: 50,

            },

            "siglas_equi": {
              required: true,
              minlength: 3,
              maxlength: 15
            },

            "fundacion_equi": {
              required: true,
              minlength: 1,
              maxlength: 4

            },

            "region_equi": {
              required: true

            },

            "numero_titulos_equi": {
              required: true,
              minlength: 1,
              maxlength: 4

            }
        },
        messages: {

            "nombre_equi": {
                required: "Debe ingresar el nombre del equipo",
                minlength: "El nombre debe tener minimo 5 caracteres",
                maxlength: "El nombre no puede tener mas de 50 caracteres"

            },
            "siglas_equi": {
              required: "Debe ingresar las siglas",
              minlength: "Las siglas debe tener minimo 5 caracteres",
              maxlength: "Las siglas no puede tener mas de 50 caracteres"

            },
            "fundacion_equi": {
              required: "Por favor, ingrese la fundación",
              minlength: "La fundacion debe tener minimo 4 caracteres",
              maxlength: "La fundacion no puede tener mas de 4 caracteres"
            },

            "region_equi": {
              required: "Por favor, ingrese la region"

            },

            "numero_titulos_equi": {
              required: "Por favor, ingrese el numero de titulos",
              minlength: "El numero debe tener minimo 1 caracteres",
              maxlength: "El numero no puede tener mas de 3 caracteres"
            }




        }
    });
});


</script>
<style media="screen">
    input {
        color: black !important;
    }
</style>
